-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : jeu. 03 fév. 2022 à 22:03
-- Version du serveur :  10.4.18-MariaDB
-- Version de PHP : 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `selectioncda`
--

-- --------------------------------------------------------

--
-- Structure de la table `lienposttopic`
--

CREATE TABLE `lienposttopic` (
  `id_post` int(11) NOT NULL,
  `id_topic` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `lienposttopic`
--

INSERT INTO `lienposttopic` (`id_post`, `id_topic`) VALUES
(1, 2),
(2, 2),
(3, 2),
(4, 1);

-- --------------------------------------------------------

--
-- Structure de la table `post`
--

CREATE TABLE `post` (
  `id_post` int(11) NOT NULL,
  `content_post` varchar(100) NOT NULL,
  `author_post` varchar(100) NOT NULL,
  `date_post` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `post`
--

INSERT INTO `post` (`id_post`, `content_post`, `author_post`, `date_post`) VALUES
(1, 'Bonjour quel-est le meilleur IDE pour développer en JAVA. Merci', 'Mohamed-Amine BENFRID', '2022-02-03 17:20:00'),
(2, 'Je te conseille netbeans ;)', 'Olivier Eklou', '2022-02-03 18:31:10'),
(3, 'Bonjour, moi je te conseillerai plutot eclips !! ', 'Juan Guttierez', '2022-02-03 18:45:40'),
(4, 'Bonjour, qui peut m\'aider a regler ce problème. Voila ce que ca affiche en compilant mon code : << N', 'Michel Cros', '2022-02-03 18:50:40');

-- --------------------------------------------------------

--
-- Structure de la table `topic`
--

CREATE TABLE `topic` (
  `topic_id` int(11) NOT NULL,
  `topic_title` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `topic`
--

INSERT INTO `topic` (`topic_id`, `topic_title`) VALUES
(1, 'erreur projet java !'),
(2, 'Le meilleur IDE JAVA');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `lienposttopic`
--
ALTER TABLE `lienposttopic`
  ADD PRIMARY KEY (`id_post`,`id_topic`),
  ADD KEY `id_topic` (`id_topic`);

--
-- Index pour la table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id_post`);

--
-- Index pour la table `topic`
--
ALTER TABLE `topic`
  ADD PRIMARY KEY (`topic_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `post`
--
ALTER TABLE `post`
  MODIFY `id_post` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `topic`
--
ALTER TABLE `topic`
  MODIFY `topic_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `lienposttopic`
--
ALTER TABLE `lienposttopic`
  ADD CONSTRAINT `lienposttopic_ibfk_1` FOREIGN KEY (`id_post`) REFERENCES `post` (`id_post`),
  ADD CONSTRAINT `lienposttopic_ibfk_2` FOREIGN KEY (`id_topic`) REFERENCES `topic` (`topic_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
