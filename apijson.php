<?php
header('Content-Type: application/json');

//CONTENU

$api = [];

try{
    //OPTION PDO CONNEXION --- A CHANGER !!
    $pdo = new PDO('mysql:host=localhost;port=3306;dbname=selectioncda;', 'root', '');
    $api["success"] = true;
    $api["message"] = "connexion à la bdd reussi";
} catch(Exception $e) {
    $api["success"] = false;
    $api["message"] = "connexion à la bdd impossible";
}

//REQUETE
$q = $pdo->prepare("SELECT post.id_post, post.content_post, post.author_post, post.date_post, topic.topic_title FROM post INNER JOIN lienposttopic ON post.id_post = lienposttopic.id_post INNER JOIN topic ON lienposttopic.id_topic = topic.topic_id");
$q->execute();
$res = $q->fetchAll();
// var_dump($res);


// AFFICHAGE
$api["success"] = true;
$api["message"] = "Liste des postes :";
$api["results"] ["postes "] = $res;

//ENCODAGE
echo json_encode($api);

?>